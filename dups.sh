#!/usr/bin/sh
# Version that is not 100 lines long
if [ "$#" -ne 2 ]; then
    echo "usage: dups <f1> <f2>"
    exit
fi

one=$(cat $1)
two=$(cat $2)

if [ "$one" = "$two" ]; then
    echo "dups"
else
    echo "not dups"
fi
