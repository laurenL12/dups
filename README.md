Find out if two files are duplicates

## Building
```console
$ git clone https://gitlab.com/laurenL12/dups.git
$ cd dups/
$ gcc dups.c -o dups
```

## License
GPL v2.0
