// dups: find duplicates
// Written by Laurentino Luna in 2022
#include <stdio.h>
#include <stdlib.h>

// Load file stream to memory
unsigned char *loadf(FILE *f, size_t *size)
{
    unsigned char *data;
    size_t fsize;

    fseek(f, 0L, SEEK_END);
    fsize = ftell(f);
    fseek(f, 0L, SEEK_SET);

    data = malloc(fsize);
    if (data == NULL)
        return NULL;

    fread(data, sizeof(unsigned char), fsize, f);
    *size = fsize;

    return data;
}

int min(int a, int b)
{
    if (a < b)
        return a;

    return b;
}

int main(int argc, char **argv)
{
    if (argc < 3) {
        printf("usage: dups <file1> <file2>\n");
        return 1;
    }

    FILE *f1, *f2;
    char *f1name, *f2name;
    unsigned char *f1d, *f2d;
    size_t f1size, f2size;

    f1name = argv[1];
    f2name = argv[2];

    f1 = fopen(f1name, "r");
    if (f1 == NULL) {
        printf("dups: file '%s' does not exist\n", f1name);
        return 1;
    }

    f2 = fopen(argv[2], "r");
    if (f2 == NULL) {
        printf("dups: file '%s' does not exist\n", f2name);
        goto cleanup;
    }

    f1d = loadf(f1, &f1size);
    if (f1d == NULL) {
        printf("dups: failed to load file '%s'\n", f1name);
        goto cleanup;
    }

    f2d = loadf(f2, &f2size);
    if (f2d == NULL) {
        printf("dups: failed to load file '%s'\n", f2name);
        goto cleanup;
    }

    int cmplimit;
    cmplimit = min(f1size, f2size);
    for (int i = 0; i < cmplimit; i++) {
        if (f1d[i] != f2d[i]) {
            printf("dups: not duplicates\n");
            goto cleanup;
        }
    }

    printf("dups: duplicates\n");
    goto cleanup;

    return 0;

cleanup:
    free(f1d);
    free(f2d);

    if (f1 != NULL)
        fclose(f1);
    if (f2 != NULL)
        fclose(f2);
    return 1;
}

